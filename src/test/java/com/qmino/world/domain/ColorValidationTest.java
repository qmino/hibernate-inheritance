package com.qmino.world.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

public class ColorValidationTest {

    private Validator validator;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void descriptionCanNotBeNull() {
        Color color = new Color(null, "#FFFFFF");

        Set<ConstraintViolation<Color>> violations = validator.validate(color);
        assertEquals(1, violations.size());

        ConstraintViolation<Color> violation = violations.iterator().next();

        assertEquals("description", violation.getPropertyPath().toString());
        assertNull(violation.getInvalidValue());
    }

    @Test
    public void descriptionCanNotBeEmpty() {
        Color color = new Color("", "#FFFFFF");

        Set<ConstraintViolation<Color>> violations = validator.validate(color);
        assertEquals(1, violations.size());

        ConstraintViolation<Color> violation = violations.iterator().next();

        assertEquals("description", violation.getPropertyPath().toString());
        assertEquals("", violation.getInvalidValue());
    }

    @Test
    public void hexCanNotBeNull() {
        Color color = new Color("white", null);

        Set<ConstraintViolation<Color>> violations = validator.validate(color);
        assertEquals(1, violations.size());

        ConstraintViolation<Color> violation = violations.iterator().next();

        assertEquals("hex", violation.getPropertyPath().toString());
        assertNull(violation.getInvalidValue());
    }

    @Test
    public void hexCanNotBeEmpty() {
        Color color = new Color("white", "");

        Set<ConstraintViolation<Color>> violations = validator.validate(color);
        assertEquals(1, violations.size());

        ConstraintViolation<Color> violation = violations.iterator().next();

        assertEquals("hex", violation.getPropertyPath().toString());
        assertEquals("", violation.getInvalidValue());
    }
}
