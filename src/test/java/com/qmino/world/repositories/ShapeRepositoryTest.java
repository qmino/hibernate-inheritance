package com.qmino.world.repositories;

import com.qmino.world.Application;
import com.qmino.world.domain.Color;
import com.qmino.world.domain.Rectangle;
import com.qmino.world.domain.Shape;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@Transactional
public class ShapeRepositoryTest {

    @Autowired
    private ShapeRepository repository;

    @Test
    public void ShouldSaveShapeWhenValid() {
        Shape toSave = new Rectangle.Builder()
                .color(new Color("white", "#FFFFFF"))
                .length(BigDecimal.TEN)
                .width(BigDecimal.TEN)
                .build();

        repository.saveAndFlush(toSave);
    }

    @Test(expected = ConstraintViolationException.class)
    public void throwsExceptionWhenShapeIsNotValid() {
        Shape toSave = new Rectangle.Builder()
                .color(null)
                .length(BigDecimal.TEN)
                .width(BigDecimal.TEN)
                .build();

        repository.saveAndFlush(toSave);
    }

}
