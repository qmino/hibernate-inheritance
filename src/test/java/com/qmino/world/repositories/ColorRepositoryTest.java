package com.qmino.world.repositories;

import com.qmino.world.Application;
import com.qmino.world.domain.Color;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@Transactional
public class ColorRepositoryTest {

    @Autowired
    private ColorRepository repository;

    @Test
    public void ShouldSaveColorWhenValid() {
        Color toSave = new Color("white", "#FFFFFF");
        repository.saveAndFlush(toSave);
    }

    @Test(expected = ConstraintViolationException.class)
    public void throwsExceptionWhenColorIsNotValid() {
        Color toSave = new Color("white", null);
        repository.saveAndFlush(toSave);
    }
}
