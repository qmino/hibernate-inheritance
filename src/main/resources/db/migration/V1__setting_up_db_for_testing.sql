CREATE SCHEMA world;

CREATE SEQUENCE color_id_seq;
CREATE SEQUENCE shape_id_seq;

CREATE TABLE color
(
    id          BIGINT DEFAULT color_id_seq.nextval PRIMARY KEY,

    description VARCHAR(255) NOT NULL,
    hex         VARCHAR(255) NOT NULL,
);

CREATE TABLE shape
(
    id       BIGINT DEFAULT shape_id_seq.nextval PRIMARY KEY,
    color_id INTEGER REFERENCES color (id),

    type     VARCHAR(31),
    length   DOUBLE PRECISION,
    width    DOUBLE PRECISION
);
