package com.qmino.world.domain;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@DiscriminatorValue(value = "rectangle")
public class Rectangle extends Shape {

    @Column
    @NotNull
    private BigDecimal length;

    @Column
    @NotNull
    private BigDecimal width;

    public Rectangle(Color color,
                     BigDecimal length,
                     BigDecimal width) {
        super(color);
        this.length = length;
        this.width = width;
    }

    private Rectangle(Builder builder) {
        super(builder.color);
        setId(builder.id);
        setLength(builder.length);
        setWidth(builder.width);
    }

    public BigDecimal getLength() {
        return length;
    }

    public void setLength(BigDecimal length) {
        this.length = length;
    }

    public BigDecimal getWidth() {
        return width;
    }

    public void setWidth(BigDecimal width) {
        this.width = width;
    }

    @Override
    public BigDecimal getArea() {
        return this.length.multiply(this.width);
    }


    public static final class Builder {
        private Integer id;
        private Color color;
        private BigDecimal length;
        private BigDecimal width;

        public Builder() {
        }

        public Builder id(Integer val) {
            id = val;
            return this;
        }

        public Builder color(Color val) {
            color = val;
            return this;
        }

        public Builder length(BigDecimal val) {
            length = val;
            return this;
        }

        public Builder width(BigDecimal val) {
            width = val;
            return this;
        }

        public Rectangle build() {
            return new Rectangle(this);
        }
    }
}
